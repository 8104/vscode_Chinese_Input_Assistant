import * as vscode from 'vscode'

class 配置 {
  constructor() {}

  public get 使用输入法(): 'no' | 'Google Pinyin' | 'Baidu Sugrec' {
    return vscode.workspace.getConfiguration('ChineseInputAssistant').get('InputMethod') as any
  }
  public get markdown补全(): 'no' | 'yes' {
    return vscode.workspace.getConfiguration('ChineseInputAssistant').get('markdownUse') as any
  }
  public get 提示方式():
    | '全拼'
    | '五笔98全码'
    | '五笔98四码'
    | '五笔86全码'
    | '五笔86四码'
    | '小鹤双拼'
    | '搜狗双拼'
    | '微软双拼'
    | '自然码双拼'
    | '紫光双拼'
    | '拼音加加双拼' {
    return vscode.workspace.getConfiguration('中文代码快速补全').get('提示方式') as any
  }
  public get 分析vsc提供的补全项(): 'yes' | 'no' {
    return vscode.workspace.getConfiguration('ChineseInputAssistant').get('useCompletionItem') as any
  }
  public get 分析当前文档的词作为补全项(): 'yes' | 'no' {
    return vscode.workspace.getConfiguration('ChineseInputAssistant').get('useFileWord') as any
  }
  public get 代理地址(): string {
    return vscode.workspace.getConfiguration('ChineseInputAssistant').get('proxy') as any
  }
}

export function 获得配置() {
  return new 配置()
}
