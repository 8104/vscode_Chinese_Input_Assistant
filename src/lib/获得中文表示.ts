import 拼音 from 'pinyin/lib/pinyin-web'
import 五笔 from './五笔'
import { 多重笛卡尔积 } from './数组扩展'
import { 转换为大写 } from './字符串扩展'
import 拼音A from './拼音'

export function 获得中文字符表示(表示方法: string, 文本: string) {
  if (表示方法.indexOf('五笔') != -1) {
    return [五笔(文本, 表示方法)]
  } else {
    var 结果 = 拼音(文本, { heteronym: true, style: 拼音.STYLE_NORMAL })
    结果 = 结果.map((a) => a.map((a) => 转换为大写(a[0]) + a.substring(1)))
    var 转换后结果 = 多重笛卡尔积(结果).map((a) => a.join(''))
    if (表示方法 != '全拼') 转换后结果 = 转换后结果.map((a) => 拼音A.双拼转换(a, 表示方法))
    return 转换后结果
  }
}
