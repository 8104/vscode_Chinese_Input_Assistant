export var 转换为大写 = (字符串: string) => 字符串.toLocaleUpperCase()
export var 包含中文 = function 包含中文(str: string) {
  return /[\u4e00-\u9fa5\u3007]/.test(str)
}
export var 是纯字母 = function 是纯字母(str: string) {
  return /[A-Za-z]/.test(str)
}
export var 标识符模式 =
  /(-?\d*\.\d\w*)|([^\`\~\!\@\^\&\*\(\)\-\#\?\=\+\[\{\]\}\\\|\;\:\'\"\,\.\<\>\/\s？。，、；：？…—·ˉˇ¨“”～〃｜《》〔〕（），]+)/g
export var 查找字段 = function 查找字段(s: string) {
  var wordPattern = 标识符模式
  return Array.from(new Set(s.match(wordPattern)))
}
export function 拆分中英文部分(输入字段: string) {
  var 非英文部分 = 输入字段
    .split('')
    .filter((a) => !是纯字母(a))
    .join('')
  var 英文部分 = 输入字段
    .split('')
    .filter((a) => 是纯字母(a))
    .join('')
  return { 非英文部分, 英文部分 }
}
