import { HTTP请求 } from './HTTP请求'
import { 拆分中英文部分 } from './字符串扩展'
import * as vscode from 'vscode'

interface 输入法 {
  输入(字符串: string, 数量: number, 代理地址: string | null): Promise<string[]>
}

class 百度联想 implements 输入法 {
  async 输入(字符串: string, 数量: number, 代理地址: string | null): Promise<string[]> {
    if (!字符串.trim()) return []
    var url = `https://www.baidu.com/sugrec?prod=pc&from=pc_web&wd=${字符串}`
    var res = await HTTP请求('GET', encodeURI(url), 代理地址, {})
    var r = JSON.parse(res)
    if (r.g == null) return []
    r = r.g.map((a: any) => a.q).filter((a: any, i: any) => i < 数量)
    return r
  }
}

class 谷歌输入法 implements 输入法 {
  // api来自 https://github.com/zyctree/vscode-google-pinyin
  async 输入(字符串: string, 数量: number, 代理地址: string | null): Promise<string[]> {
    if (字符串.indexOf('\r') != -1 || 字符串.indexOf('\n') != -1) return []
    var url = `http://inputtools.google.com/request?text=${字符串}&itc=zh-t-i0-pinyin&num=${数量}&cp=0&cs=1&ie=utf-8&oe=utf-8&app=demopage`
    var res = await HTTP请求('POST', encodeURI(url), 代理地址, {})
    var r = JSON.parse(res)
    return r[1][0][1]
  }
}

export async function 获得输入法补全项(
  输入法: 'no' | 'Google Pinyin' | 'Baidu Sugrec',
  内容: string,
  代理地址: string,
) {
  var 输入法对象: 输入法
  if (输入法 == 'Baidu Sugrec') 输入法对象 = new 百度联想()
  else if (输入法 == 'Google Pinyin') 输入法对象 = new 谷歌输入法()
  else return []

  // 如果分段输入一个词组，例如“多重笛卡尔积”，输入顺序是：先输入“多重”，再输入“笛卡尔积”。
  // 当输入到“多重d”的时候，变量`输入字段`值为“多重d”。如果直接拿这个词去调用输入法，很难得到正确的结果。
  // 但如果使用"d"去调用输入法，得到的结果并不以"多重"开头，导致不会弹出提示框。
  // 因此，这里的方案是，拆分非英文部分和英文部分，用英文部分调用输入法，再在结果前面补上非英文部分。
  // 注意：这个策略在一些情况下会失效，例如在中文当中输入时。
  var { 非英文部分, 英文部分 } = 拆分中英文部分(内容)
  var 输入法提供的词 = await 输入法对象.输入(英文部分, 5, 代理地址)
  var 输入法提供的词项 = 输入法提供的词
    .map((a) => 非英文部分 + a)
    .map((a) => new vscode.CompletionItem(a, vscode.CompletionItemKind.Text))
  return 输入法提供的词项
}
